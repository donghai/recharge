/***************************************************
 ** @Desc : This file for 加密数据模型
 ** @Time : 2019.04.11 16:25
 ** @Author : Joker
 ** @File : data
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.11 16:25
 ** @Software: GoLand
****************************************************/
package models

// 先锋充值加密数据
type XFRechargeData struct {
	MerchantNo  string `json:"merchantNo"`  //商户订单号
	Amount      string `json:"amount"`      //金额
	TransCur    string `json:"transCur"`    //币种
	AccountNo   string `json:"accountNo"`   //账户号
	AccountName string `json:"accountName"` //账户名称
	RecevieBank string `json:"recevieBank"` //备付金账户标识
	NoticeUrl   string `json:"noticeUrl"`   //后台通知地址
}

// 先锋充值查询加密数据
type XFRechargeQueryData struct {
	MerchantNo string `json:"merchantNo"` //商户订单号
}

// 先锋余额查询加密数据
type XFBalanceQueryData struct {
	MerchantId string `json:"merchantId"` //商户编号
}

type TerminalParams struct {
	MerEquipmentIp string `json:"merEquipmentIp"` //ip
}

// 先锋代付加密数据
type XFPayData struct {
	MerchantNo     string `json:"merchantNo"`     //商户订单号
	Source         string `json:"source"`         //来源
	Amount         string `json:"amount"`         //金额
	TransCur       string `json:"transCur"`       //币种
	UserType       string `json:"userType"`       //用户类型
	AccountNo      string `json:"accountNo"`      //账户号
	AccountName    string `json:"accountName"`    //账户名称
	AccountType    string `json:"accountType"`    //账户类型
	MobileNo       string `json:"mobileNo"`       //手机号
	BankNo         string `json:"bankNo"`         //银行编码
	Issuer         string `json:"issuer"`         //联行号
	BranchProvince string `json:"branchProvince"` //开户省
	BranchCity     string `json:"branchCity"`     //开户市
	BranchName     string `json:"branchName"`     //开户支行名称
	TradeProduct   string `json:"tradeProduct"`   //开户支行名称
	NoticeUrl      string `json:"noticeUrl"`      //后台通知地址
	Memo           string `json:"memo"`           //保留域
	TerminalParams string `json:"terminalParams"` //终端信息
}

// b2c转账加密数据
type B2CTransferData struct {
	MerchantNo     string `json:"merchantNo"`     //商户订单号
	Source         string `json:"source"`         //来源
	ProductType    string `json:"productType"`    //产品类型
	PayerId        string `json:"payerId"`        //付款方
	Amount         string `json:"amount"`         //金额
	TransCur       string `json:"transCur"`       //币种
	AccountType    string `json:"accountType"`    //卡种
	ProductName    string `json:"productName"`    //商品名称
	ProductInfo    string `json:"productInfo"`    //商品信息
	ReturnUrl      string `json:"returnUrl"`      //前台通知地址
	NoticeUrl      string `json:"noticeUrl"`      //后台通知地址
	ExpireTime     string `json:"expireTime"`     //订单超时时间
	Memo           string `json:"memo"`           //保留域
	TerminalParams string `json:"terminalParams"` //终端信息
}

// 充值同步响应数据
type RespData struct {
	Code       string `json:"code"`
	Message    string `json:"message"`
	MerchantId string `json:"merchantId"`
	Data       string `json:"data"`
	Tm         string `json:"tm"`
	Sign       string `json:"sign"`
}
