package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/dchest/captcha"
	"recharge/controllers"
	"recharge/sys"
)

func init() {
	//生产登录验证码
	beego.Handler("/captcha/*.png", captcha.Server(130, 40))

	beego.Include(&controllers.UserLogin{})
	beego.Include(&controllers.XFMerchant{})
	beego.Include(&controllers.UserManage{})
	beego.Include(&controllers.Recharge{})
	beego.Include(&controllers.Pay{})
	beego.Include(&controllers.Record{})
	beego.Include(&controllers.RechargeQuery{})
	beego.Include(&controllers.PayQuery{})
	beego.Include(&controllers.AsyNotice{})
	beego.Include(&controllers.Transfer{})
	beego.Include(&controllers.BulkPay{})
	beego.Include(&controllers.UserReconcile{})
	beego.Include(&controllers.ApiRecharge{})
	beego.Include(&controllers.ApiPay{})
	beego.Include(&controllers.BulkRecharge{})
	beego.Include(&controllers.UCustomPlusMinus{})
	beego.Include(&controllers.RechargeV2{})
	beego.Include(&controllers.PayV2{})
	beego.Include(&controllers.AsyNoticeV2{})
	beego.Include(&controllers.RechargeQueryV2{})
	beego.Include(&controllers.PayQueryV2{})
	beego.Include(&controllers.HomeAct{})

	//过滤器,判断是否登录状态
	beego.InsertFilter("/merchant/*", beego.BeforeRouter, IndexFill)
	//权限控制
	beego.InsertFilter("/user/*", beego.BeforeRouter, UserFill)

	//b2c控制
	beego.InsertFilter("/transfer/*", beego.BeforeRouter, UserFillForB2C)
}

var IndexFill = func(ctx *context.Context) {
	userName := ctx.Input.Session("FilterUsers")
	if userName == nil {
		sys.LogNotice("未经登录，直接非法操作URI。管理注意了，可能有人在搞事！")
		ctx.Redirect(302, "/IsRight")
	}
}

var UserFill = func(ctx *context.Context) {
	right := ctx.Input.Session("FilterRight")
	if right == nil {
		sys.LogNotice("未经登录或权限不够，直接非法操作URI。管理注意了，可能有人在搞事！")
		ctx.Redirect(302, "/IsRight")
	} else if right.(int) > 0 {
		sys.LogNotice("未经登录且权限不够，直接非法操作URI。管理注意了，可能有人在搞事！")
		ctx.Redirect(302, "/IsRight")
	}
}

var UserFillForB2C = func(ctx *context.Context) {
	right := ctx.Input.Session("FilterRight")
	if right == nil {
		sys.LogNotice("未经登录或没有B2C权限，直接非法操作URI。管理注意了，可能有人在搞事！")
		ctx.Redirect(302, "/IsRight")
	} else if right.(int) > 1 {
		sys.LogNotice("未经登录且没有B2C权限，直接非法操作URI。管理注意了，可能有人在搞事！")
		ctx.Redirect(302, "/IsRight")
	}
}
