/***************************************************
 ** @Desc : This file for 维持session
 ** @Time : 2019.04.17 9:10 
 ** @Author : Joker
 ** @File : keep_session
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.17 9:10
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/session"
)

type KeepSession struct {
	beego.Controller
}

// 若用户在30分钟内没有操作行为，则删除session信息
func (the *KeepSession) Prepare() {
	loginName := the.GetSession("FilterUsers")
	if loginName != nil {
		u := userMdl.SelectOneUser(loginName.(string))
		the.SetSession("FilterUsers", u.LoginName)
		the.SetSession("FilterRight", u.Authority)
		the.SetSession("userName", u.UserName)
		the.SetSession("userId", u.Id)

		sessionConfig := &session.ManagerConfig{
			CookieName:      "JOKERSession",
			EnableSetCookie: true,
			Gclifetime:      3600,
			Maxlifetime:     3600,
			Secure:          false,
			CookieLifeTime:  3600,
			ProviderConfig:  "./temp",
		}
		_, _ = session.NewManager("file", sessionConfig)
	}
}
