/***************************************************
 ** @Desc : This file for 异步通知
 ** @Time : 2019.04.11 18:35 
 ** @Author : Joker
 ** @File : notice
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.11 18:35
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"encoding/json"
	"recharge/controllers/implement"
	"recharge/models"
	"recharge/sys"
	"recharge/utils"
	"strings"
)

type AsyNotice struct {
	KeepSession
}

var apiNotice = implement.ApiAsyNotice{}

// 先锋充值异步通知
// @router /notice/xf_recharge_notice/?:params [get,post]
func (the *AsyNotice) XFRechargeNotice() {
	orderId := the.GetString(":params")
	record := rechargeMdl.SelectOneRechargeRecordByOrderId(orderId)

	if record.MerchantNo == "" || record.Id == 0 {
		sys.LogInfo("先锋充值异步非法通知，订单不存在:", orderId)
	} else if strings.Compare(utils.S, record.Status) == 0 {
		sys.LogInfo("先锋充值订单:", orderId, "异步重复通知，请忽略")
	} else {
		merchant := merchantMdl.SelectOneMerchantByNo(record.MerchantNo)

		data := the.GetString("data")
		bytes, err := AES.AesDecrypt([]byte(data), []byte(merchant.SecretKey))
		if err != nil {
			s := "先锋充值异步响应解密错误"
			sys.LogTrace(s, err)
		} else {

			resp := models.XFRechargeResponseBody{}
			err = json.Unmarshal(bytes, &resp)
			if err != nil {
				sys.LogTrace("先锋充值异步响应数据格式错误:", err)
			} else {

				// 生成签名
				//respParams := map[string]string{}
				//respParams["merchantId"] = resp.MerchantId
				//respParams["merchantNo"] = resp.MerchantNo
				//respParams["tradeNo"] = resp.TradeNo
				//respParams["status"] = resp.Status
				//respParams["tradeTime"] = resp.TradeTime
				//respParams["memo"] = resp.Memo
				//respParams["resCode"] = resp.ResCode
				//respParams["resMessage"] = resp.ResMessage
				//params := globalMethod.ToStringByMap(respParams)

				record.EditTime = globalMethod.GetNowTime()
				record.Remark = resp.ResMessage

				if strings.Compare(utils.S, resp.Status) == 0 {
					record.Status = utils.S
					// 加款
					addition := userMdl.SelectOneUserAddition(record.UserId, record.ReAmount, record)
					if addition > 0 {
						//若是对接订单,发送异步通知
						if strings.Compare(utils.A, record.RecordClass) == 0 {
							apiNotice.ApiRechargeAsyNotice(record)
						}

						sys.LogInfo("先锋充值订单:", record.ReOrderId, "加款成功")
					} else {
						sys.LogInfo("先锋充值订单:", record.ReOrderId, "加款失败")
					}
				} else if strings.Compare(utils.F, resp.Status) == 0 {
					record.Status = utils.F
					rechargeMdl.UpdateRechargeRecord(record)
				}
			}
		}
	}

	the.Ctx.WriteString("SUCCESS")
	the.StopRun()
}

// 先锋代付异步通知
// @router /notice/xf_pay_notice/?:params [get,post]
func (the *AsyNotice) XFPayNotice() {
	orderId := the.GetString(":params")
	record := payMdl.SelectOneWithdrawRecordByOrderId(orderId)

	if record.MerchantNo == "" || record.Id == 0 {
		sys.LogInfo("先锋代付异步非法通知，订单不存在:", orderId)
	} else if strings.Compare(utils.S, record.Status) == 0 {
		sys.LogInfo("先锋代付订单:", orderId, ",异步重复通知，请忽略")
	} else {

		merchant := merchantMdl.SelectOneMerchantByNo(record.MerchantNo)

		data := the.GetString("data")
		bytes, err := AES.AesDecrypt([]byte(data), []byte(merchant.SecretKey))
		if err != nil {
			s := "先锋代付异步响应解密错误"
			sys.LogTrace(s, err)
		} else {

			resp := models.XFPayResponseBody{}
			err = json.Unmarshal(bytes, &resp)
			if err != nil {
				sys.LogTrace("先锋代付异步响应数据格式错误:", err)
			} else {

				// 生成签名
				//respParams := map[string]string{}
				//respParams["merchantId"] = resp.MerchantId
				//respParams["merchantNo"] = resp.MerchantNo
				//respParams["amount"] = resp.TradeNo
				//respParams["transCur"] = resp.TradeNo
				//respParams["tradeNo"] = resp.TradeNo
				//respParams["tradeTime"] = resp.TradeTime
				//respParams["status"] = resp.Status
				//respParams["memo"] = resp.Memo
				//respParams["resCode"] = resp.ResCode
				//respParams["resMessage"] = resp.ResMessage
				//params := globalMethod.ToStringByMap(respParams)

				record.EditTime = globalMethod.GetNowTime()
				record.Remark = resp.ResMessage

				if strings.Compare(utils.S, resp.Status) == 0 {
					record.Status = utils.S
					WhAmount, _ := globalMethod.MoneyYuanToFen(record.WhAmount)
					//比较金额
					if strings.Compare(WhAmount, resp.Amount) == 0 {
						// 减款
						addition := userMdl.SelectOneUserDeductionForSuccess(record.UserId, record.WhAmount, record)
						if addition > 0 {
							//若是对接订单,发送异步通知
							if strings.Compare(utils.A, record.RecordClass) == 0 {
								apiNotice.ApiPayAsyNotice(record)
							}

							sys.LogInfo("先锋代付订单:", record.WhOrderId, "减款成功")
						} else {
							sys.LogInfo("先锋代付订单:", record.WhOrderId, "减款失败")
						}

						//发送提现通知
						/*info, _ := userMdl.SelectOneUserById(record.UserId)
						sms.SendSmsForPay(utils.MOBILE, info.UserName)*/
					}
				} else if strings.Compare(utils.F, resp.Status) == 0 {
					record.Status = utils.F
					userMdl.SelectOneUserDeductionForFail(record.UserId, record.WhAmount, record)
				}
			}
		}
	}

	the.Ctx.WriteString("SUCCESS")
	the.StopRun()
}

// b2c转账异步通知
// @router /notice/b2c_transfer_notice/?:params [get,post]
func (the *AsyNotice) B2CTransferNotice() {
	orderId := the.GetString(":params")
	record := transferMdl.SelectOneTransferRecordByOrderId(orderId)

	if record.MerchantNo == "" || record.Id == 0 {
		sys.LogInfo("b2c转账异步非法通知，订单不存在:", orderId)
	} else if strings.Compare(utils.S, record.Status) == 0 {
		sys.LogInfo("b2c转账订单:", orderId, ",异步重复通知，请忽略")
	} else {

		merchant := merchantMdl.SelectOneMerchantByNo(record.MerchantNo)

		data := the.GetString("data")
		bytes, err := AES.AesDecrypt([]byte(data), []byte(merchant.SecretKey))
		if err != nil {
			s := "b2c转账异步响应解密错误"
			sys.LogTrace(s, err)
		} else {

			resp := models.XFPayResponseBody{}
			err = json.Unmarshal(bytes, &resp)
			if err != nil {
				sys.LogTrace("b2c转账异步响应数据格式错误:", err)
			} else {

				record.EditTime = globalMethod.GetNowTime()
				record.Status = resp.Status

				if strings.Compare(utils.S, resp.Status) == 0 {
					// 加款
					addition := userMdl.SelectOneUserAdditionNotFee(record.UserId, record.TrAmount, record)
					if addition > 0 {
						sys.LogInfo("b2c转账订单:", record.TrOrderId, "加款成功")
					} else {
						sys.LogInfo("b2c转账订单:", record.TrOrderId, "加款失败")
					}
				} else if strings.Compare(utils.F, resp.Status) == 0 {
					transferMdl.UpdateTransferRecord(record)
				}
			}
		}
	}

	the.Ctx.WriteString("SUCCESS")
	the.StopRun()
}
